# COMED Gateway Infrastructure

ABP Framework ile ayakta çalışan bir .NET Core uygulamadır.

## Task
HTTP üzerinden Gateway'e gelen isteği abstract eder.

- `CorrelationId`: İsteğin unique id değeri
- `Data`: İstek ile gelen payload
- `OperationTypeCode`: Gelen isteğin tip kodu. (Ör: InsertAppointment, GetVisits vs.)

## TaskManager 

Gelen HTTP `Task`'ları alıp config'lerden resolve ettiği `ITaskRunner` 'ın ilgili metodunu tetikleyen engine. Cevabı da yine `ResponseConfig`'e bakarak hazırlar ve return eder.


## OperationTypeConfiguration

- `OperationTypeCode`
- `TaskRunnerType` (HTTP, SQL)
- `Config` (serialized json config)
	- `IHttpTaskConfiguration` veya `ISqlTaskConfiguration`
- `ResponseType` (Concurrent, Queue)
- `ResponseConfig`
	- `IHttpTaskConfig`

## IHttpTaskConfiguration

- Url
- HttpMethod
- AuthConfig
- Headers

## IHttpTaskRunner

- IHttpTaskConfiguration
- Data
- RunTask
- RunTaskAsync

## ISqlTaskConfiguration

- DbConnectionStr
- Parameters (Dictionary)

## ISqlTaskRunner

- ISqlTaskConfiguration
- RunTask
- RunTaskAsync

# Faz - 1 Çalışmalar


## Gateway Application

- ABP Framework ile hazırlanacak
- MVC UI Template ile başlanacak

## Servisler

### TaskService
Bu servis taskları kabul eden API bacağı olacak

## Manager

- TaskManager
- HttpTaskRunner (IHttpTaskRunner)
- SqlTaskRunner (ISqlTaskRunner)

## Entity

- OperationTypeConfiguration
- IHttpTaskConfiguration
- ISqlTaskConfiguration

